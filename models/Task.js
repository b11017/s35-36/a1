const { default: mongoose } = require("mongoose");

const taskSchema = new mongoose.Schema({
   
    name : String,
   
    status : String
});

module.exports = mongoose.model("Task", taskSchema);

